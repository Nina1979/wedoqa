package com.wedoqa.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class PageObject {
	protected WebDriver driver;

	public PageObject(WebDriver driver) {
		//passing an instance of the driver for initialitization of web elements
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void waitForVisibility(WebElement element) 
	{
		this.waitForVisibility(element, 10);
	}

	public void waitForVisibility(WebElement element, Integer waitTime) 
	{
		new WebDriverWait(driver, waitTime).until(ExpectedConditions.visibilityOf(element));
	}
}
