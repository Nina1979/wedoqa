package com.wedoqa.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.test1.SearchResultPage;

public class GoogleSearchPage extends PageObject {

	@FindBy(id="lst-ib")
	private WebElement searchBox;

	public GoogleSearchPage(WebDriver driver) 
	{
		super(driver);

	}
	
	// search for a key word in google
	public SearchResultPage searchFor(String text) 
	{
		this.searchBox.clear();
		this.searchBox.sendKeys(text);
		Log.info("Key word entered");
		searchBox.submit();
		 
		return new 	SearchResultPage(driver);

	}
}
