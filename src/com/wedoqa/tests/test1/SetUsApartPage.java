package com.wedoqa.tests.test1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class SetUsApartPage extends PageObject {
	
	@FindBy(linkText="What sets us apart")
	private WebElement menuItemSetUsApart;

	public SetUsApartPage(WebDriver driver) {
		super(driver);

	}

	public void clickSetUsApart() {
		//click on menu item Home
		menuItemSetUsApart.click();
		Log.info("menu Item Set Us Apart clicked");

	}

}
