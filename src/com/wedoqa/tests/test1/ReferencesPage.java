package com.wedoqa.tests.test1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class ReferencesPage  extends PageObject {

	@FindBy(linkText="References")
	private WebElement menuItemReferences;



	public ReferencesPage(WebDriver driver) {
		super(driver);

	}

	public void clickReferences() {
		//click on menu item Home
		menuItemReferences.click();
		Log.info("menu Item References clicked");

	}
}


