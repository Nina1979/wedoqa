package com.wedoqa.tests.test1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;


public class HomePage  extends PageObject{
	
	@FindBy(linkText="Home")
	public WebElement menuItemHome;

	public HomePage(WebDriver driver) {
		super(driver);
	}


	public void clickHome() {
		//click on menu item Home
		menuItemHome.click();
		Log.info("menu Item Home clicked");
	}
}
