package com.wedoqa.tests.test1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class WhoWeArePage extends PageObject {

	@FindBy(linkText="Who we are")
	private WebElement menuItemWhoWeAre;

	@FindBy(xpath="//img[@src='img/vilmos_somogyi.png']")
	private WebElement image;

	@FindBy(css=".popover-content p")
	private WebElement popoverText;


	public WhoWeArePage(WebDriver driver) {
		super(driver);

	}
	public void clickWhoWeAre() {
		//click on menu item Home
		menuItemWhoWeAre.click();
		Log.info("menu Item Who We Are clicked");

	}
	//moving the cursor to get the focus on the element
	public void moveCursor(){
		Actions action = new Actions(driver);
		action.moveToElement(image).build().perform();

	}
	//printing the text from the popup in console
	public void printText() {
		System.out.println(popoverText.getText());

	}
}
