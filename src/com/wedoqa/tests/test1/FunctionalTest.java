package com.wedoqa.tests.test1;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.wedoqa.tests.Log;


public class FunctionalTest {

	protected static WebDriver driver;
	private static Logger Log = LogManager.getLogger(Log.class);
	
	 @BeforeClass public static void setUp(){ 

			
		//create a new instance of a chrome driver
		driver = new ChromeDriver(); 
		Log.info("New driver instantiated");
		//times out after ten seconds
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Log.info("Implicit wait applied on the driver for 30 seconds");
	}

	@After
	public void cleanUp() {
		//deleting cookies
		driver.manage().deleteAllCookies();
		Log.info("Deleting all cookies");

	}

	@AfterClass
	public static void tearDown() {
		//closing the driver
		driver.quit();
		Log.info("Browser closed");

	}
}
