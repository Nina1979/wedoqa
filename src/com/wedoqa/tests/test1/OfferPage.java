package com.wedoqa.tests.test1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.PageObject;

public class OfferPage extends PageObject{

	@FindBy(linkText="What we offer")
	private WebElement menuItemOffer;


	public OfferPage(WebDriver driver) {
		super(driver);

	}

	public void clickOffer() {
		//click on menu item Home
		menuItemOffer.click();

	}


}
