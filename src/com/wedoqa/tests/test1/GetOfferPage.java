package com.wedoqa.tests.test1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class GetOfferPage extends PageObject {
	
	@FindBy(linkText="Get an offer")
	private WebElement menuItemGetOffer;

	public GetOfferPage(WebDriver driver) {
		super(driver);

	}
	public void clickGetOffer() {
		//click on menu item Get Offer
		menuItemGetOffer.click();
		Log.info("Menu item Get Offer clicked");

	}

}
