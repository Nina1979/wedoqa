package com.wedoqa.tests.test1;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class SearchResultPage extends PageObject{
	
	@FindBy(css="#search a")
	private WebElement searchResult;
	
	@FindBy(css="#resultStats")
	private WebElement resultStats;
	
	
	
	public SearchResultPage(WebDriver driver) {
		super(driver);
	
	}
	
	public void clickFirstResult() 
	{
		searchResult.click();
		Log.info("First result clicked");
	}
	
	public String getFirstResultText() {
		
		return searchResult.getText();
	}
	
	//extracting number of results from google result by using regex
    public String getResultStats() {
    
	     final String regex = "([\\d\\,]+)";
	     String match = null;
	     //getting the google result 
	     String string = resultStats.getText();
	     //creating a pattern
	     Pattern pattern = Pattern.compile(regex);
	     //creating a matcher
	     Matcher matcher = pattern.matcher(string);
	     //if  the match is found it is added to match variable
	     if (matcher.find()) {
	           match = matcher.group(1);            
	        } else {
	            System.out.println("NO MATCH");
	        }

	     return  match;	     
   }
}
    
    
    
    
    

