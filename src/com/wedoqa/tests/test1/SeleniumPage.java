package com.wedoqa.tests.test1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class SeleniumPage extends PageObject {

	@FindBy(linkText="Selenium")
	private WebElement menuItemSelenium;



	public SeleniumPage(WebDriver driver) {
		super(driver);

	}
	public void clickSelenium() {
		//click on menu item Home
		menuItemSelenium.click();
		Log.info("menu Item Selenium clicked");

	}
}
