package com.wedoqa.tests.test1;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

import org.apache.commons.io.FileUtils;


import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class BlogPage extends PageObject{

	@FindBy(linkText="Blog")
	private WebElement menuItemBlog;

	@FindBy(id="search")
	private WebElement searchBox;

	@FindBy(css=".info_index li.time")
	private WebElement date;

	public BlogPage(WebDriver driver) {
		super(driver);

	}
	public void clickBlog() {
		//click on menu item Blog
		menuItemBlog.click();
		Log.info("Click action performed on menu item Blog");

	}

	public String takeScreenshot() {

		String path;
		try {
			//switching to a new window
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); 
			}
			WebDriver augmentedDriver = new Augmenter().augment(driver); 
			File source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
			//creating a path for saving a screenshot file
			path = "d:\\screenshot.png";
			FileUtils.copyFile(source, new File(path));
		} catch (IOException e) {

			path = "Failed to capture screenshot: " + e.getMessage();

		}

		return path;

	}

	public void searchFor(String text) 
	{
		this.searchBox.clear();
		this.searchBox.sendKeys(text);
		Log.info("Text entered in the search text box");
		searchBox.submit();


	}
	//calculating how many days have passed since last date entry on the wedoqa blog
	public void calculation() throws ParseException {

		DateFormat format =  new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
		Date lastEntryDate =  format.parse(date.getText());
		Date dateobj = new Date();
		Date curentDate = format.parse(format.format(dateobj));
		long dif = curentDate.getTime() - lastEntryDate.getTime();
		long days= TimeUnit.MILLISECONDS.toDays(dif);
		System.out.println(String.valueOf(days));

	}


}
