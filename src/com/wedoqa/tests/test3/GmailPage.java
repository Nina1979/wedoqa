package com.wedoqa.tests.test3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class GmailPage extends PageObject{
	
	@FindBy(id="identifierId")
	private WebElement email;

	@FindBy(id="identifierNext")
	private WebElement buttonEmailNext;

	@FindBy(name="password")
	private WebElement inputPassword;

	@FindBy(id="passwordNext")
	private WebElement buttonPasswordNext;

	@FindBy(css=".bog b")
	private WebElement message;

	@FindBy(css=".a3s.aXjCH")
	private WebElement textMessage;

	public GmailPage(WebDriver driver) {
		super(driver);

	}
	public void fillEmail(String text) {
		email.clear();
		email.sendKeys(text);
		Log.info("Email address entered in the email address text box");

	}

	public void clickEmailNext() {
		//click on button Next
		buttonEmailNext.click();
		Log.info("Click action performed on button Email Next");

	}

	public void fillPassword(String input) {
		this.waitForVisibility(inputPassword);
		Log.info("Wait applied on the driver for 10 seconds in order for element to be visible");
		inputPassword.clear();
		inputPassword.sendKeys(input);
		Log.info("Password entered in the password text box");
	}
	
	public void clickPasswordNext() {
		//click on button Next
		buttonPasswordNext.click();
		Log.info("Click action performed on button Password Next");
	}
	
	public void clickMessage() {
		//wait for 5 min for a mail to arrive
		this.waitForVisibility(message,300);
		Log.info("Wait applied on the driver for 300 seconds in order for element to be visible");
		//click on the newly arrived message in inbox
		message.click();
		Log.info("Click action performed on Message element");

	}
	
	public String getTextMessage() {
		//get the text from the mail message
		return textMessage.getText();

	}

}
