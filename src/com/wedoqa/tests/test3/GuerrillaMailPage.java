package com.wedoqa.tests.test3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.wedoqa.tests.Log;
import com.wedoqa.tests.PageObject;

public class GuerrillaMailPage extends PageObject {

	@FindBy(css="#nav-item-compose .nav_not_active_a")
	private WebElement composeTab;

	@FindBy(name="to")
	private WebElement inputAddress;

	@FindBy(name="subject")
	private WebElement inputSubject;

	@FindBy(name="body")
	private WebElement inputText;

	@FindBy(id="send-button")
	private WebElement sendButton;

	@FindBy(css=".rc-anchor-center-container .recaptcha-checkbox-checkmark")
	private WebElement checkButton;


	public GuerrillaMailPage(WebDriver driver) {
		super(driver);

	}
	
	public void clickTab() {
		//click on Compose Tab
		composeTab.click();
		Log.info("Compose tab clicked");
	}
	
	public void fillForm(String address, String subject, String text) {
		//fill the mail form with mail address
		inputAddress.clear();
		inputAddress.sendKeys(address);
		Log.info("Mail address entered");
		
		//fill the mail form with subject of the mail
		inputSubject.clear();
		inputSubject.sendKeys(subject);
		Log.info("Subject entered");
		
		//fill the mail form with message of the mail
		inputText.clear();
		inputText.sendKeys(text);
		Log.info("Message of the mail entered");

	}

	public void clickButton() {
		//click on button Send
		sendButton.click();
		Log.info("Click action performed on Send button");


	}


}
