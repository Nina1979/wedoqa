package com.wedoqa.tests;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import java.text.ParseException;

import org.junit.Test;


import com.wedoqa.tests.test1.BlogPage;
import com.wedoqa.tests.test1.FunctionalTest;
import com.wedoqa.tests.test1.GetOfferPage;
import com.wedoqa.tests.test1.HomePage;
import com.wedoqa.tests.test1.OfferPage;
import com.wedoqa.tests.test1.ReferencesPage;
import com.wedoqa.tests.test1.SearchResultPage;
import com.wedoqa.tests.test1.SeleniumPage;
import com.wedoqa.tests.test1.SetUsApartPage;
import com.wedoqa.tests.test1.WhoWeArePage;
import com.wedoqa.tests.test3.GmailPage;
import com.wedoqa.tests.test3.GuerrillaMailPage;



public class WeDoQATest extends FunctionalTest {

	@Test
	public void FirstTest() {
		//navigate to google website
		driver.get("http://www.google.com");

		//create object of GoogleSaerchPage class
		GoogleSearchPage googleSearchPage = new GoogleSearchPage (driver);

		//fill up data for search
		SearchResultPage searchResultPage = googleSearchPage.searchFor("wedoqa.com");

		//check if result is wedoqa
		assertEquals("WeDoQA", searchResultPage.getFirstResultText());

		searchResultPage.clickFirstResult();

		//create an object of HomePage class
		HomePage homePage = new HomePage(driver);
		homePage.clickHome();

		//create an object of OfferPage class
		OfferPage itemOffer = new OfferPage(driver);
		itemOffer.clickOffer();

		//create an object of SetUsApartPage class
		SetUsApartPage itemSetUsApart = new SetUsApartPage(driver);
		itemSetUsApart.clickSetUsApart();

		//create an object of GetOfferPage
		GetOfferPage itemGetOfferPage = new GetOfferPage(driver);
		itemGetOfferPage.clickGetOffer();	

		//create an object of SeleniumPage
		SeleniumPage itemSelenium = new SeleniumPage(driver);
		itemSelenium.clickSelenium();	

		//create an object of ReferencesPage
		ReferencesPage itemReferences = new ReferencesPage(driver);
		itemReferences.clickReferences();

		//create an object of ReferencesPage
		WhoWeArePage itemWhoWeAre= new WhoWeArePage(driver);
		itemWhoWeAre.clickWhoWeAre();
		itemWhoWeAre.moveCursor();
		itemWhoWeAre.printText();

		//create an object of BlogPage
		BlogPage itemBlog= new BlogPage(driver);
		itemBlog.clickBlog();
		itemBlog.takeScreenshot();
		itemBlog.searchFor("test");
		try {
			itemBlog.calculation();
		} catch (ParseException e) {

			e.printStackTrace();
		}
	}

	@Test
	public void SecondTest() {
		//navigate to google website
		driver.get("http://www.google.com");

		//create object of GoogleSaerchPage class
		GoogleSearchPage googleSearchPage = new GoogleSearchPage (driver);

		//fill up data for search
		SearchResultPage searchResultPage = googleSearchPage.searchFor("cheese");

		//comparing the result with 777
		assertTrue("There is too much cheese on the internet.",searchResultPage.getResultStats().equals("777"));	
	}

	@Test
	public void ThirdTest() {

		final String mailMessage = "Hello world!!!:-)";
		final String mailSubject = "Message";
		final String mailAddress = "wedoqatest@gmail.com";
		final String mailPassword = "asdfasdf1234";


		//navigate to guerrilamail  website
		driver.get("https://www.guerrillamail.com/");

		//create an object of GuerrillaMailPage 
		GuerrillaMailPage guerillaMail= new GuerrillaMailPage(driver);
		guerillaMail.clickTab();
		guerillaMail.fillForm(mailAddress, mailSubject, mailMessage);
		guerillaMail.clickButton();
		
		//putting a thread to sleep for 5 min in order to be able to manually enter captcha 
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//navigate to google mail
		driver.get("https://mail.google.com/");

		//create an object of gmail page
		GmailPage gmail = new GmailPage(driver);
		
		//filling the form on gmail account
		gmail.fillEmail(mailAddress);
		gmail.clickEmailNext();  
		gmail.fillPassword(mailPassword);
		gmail.clickPasswordNext();
		
		//open a newly arrived message
		gmail.clickMessage();

		assertEquals(true, gmail.getTextMessage().startsWith(mailMessage));
		
	}
}



